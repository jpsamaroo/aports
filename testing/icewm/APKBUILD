# Contributor: Paul Bredbury <brebs@sent.com>
# Maintainer: Paul Bredbury <brebs@sent.com>
pkgname=icewm
pkgver=1.8.3
pkgrel=0
pkgdesc="Window manager designed for speed, usability and consistency"
url="https://github.com/ice-wm/icewm"
arch="all"
options="!check" # No test suite
license="LGPL-2.0-only"
subpackages="$pkgname-doc $pkgname-lang"
makedepends="alsa-lib-dev
	asciidoctor
	cmake
	fribidi-dev
	gdk-pixbuf-dev
	glib-dev
	libao-dev
	libintl
	libsm-dev
	libsndfile-dev
	libxcomposite-dev
	libxdamage-dev
	libxft-dev
	libxinerama-dev
	libxpm-dev
	libxrandr-dev
	perl"

case "$CARCH" in
	s390x)
		;;
	*)
		makedepends="$makedepends librsvg-dev"
		_arch_opts="-DCONFIG_LIBRSVG=ON"
		;;
esac

source="https://github.com/ice-wm/icewm/releases/download/$pkgver/icewm-$pkgver.tar.lz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCFGDIR=/etc \
		-DENABLE_NLS=OFF \
		-DENABLE_LTO=ON \
		-DDOCDIR=/usr/share/doc/icewm $_arch_opts
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="697395ff995b57e043774bae2cce667935879c4deab17d9a9059c1e152a90704ad3af618a2c945ac5afd561754895d4e9805f4fea77d5f1e06cf99a7dfd4c062  icewm-1.8.3.tar.lz"
