# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=systemsettings
pkgver=5.20.2
pkgrel=0
pkgdesc="Plasma system manager for hardware, software, and workspaces"
# armhf blocked by qt5-qtdeclarative
# s390x blocked by libksysguard
arch="all !armhf !s390x"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0-or-later"
depends="kirigami2"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kcrash-dev kitemviews-dev kcmutils-dev ki18n-dev kio-dev kservice-dev kiconthemes-dev kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev kdbusaddons-dev kconfig-dev kdoctools-dev kpackage-dev kdeclarative-dev kactivities-dev kactivities-stats-dev kirigami2-dev plasma-workspace-dev khtml-dev"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/plasma/$pkgver/systemsettings-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="044073040c6d52d4997f7315a1f6ec5e9d24d29efa9baebbc16a1907b80e4510fc412df32027bcbf8705cd871fb3ef612b768a07307c18fd0559a2cfa22431af  systemsettings-5.20.2.tar.xz"
