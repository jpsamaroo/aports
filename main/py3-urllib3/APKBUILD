# Contributor: Yura Kushnir <kushnir.yura@gmail.com>
# Maintainer: Yura Kushnir <kushnir.yura@gmail.com>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-urllib3
_pkgname=urllib3
pkgver=1.25.11
pkgrel=0
pkgdesc="HTTP library with thread-safe connection pooling, file post, and more"
url="https://github.com/shazow/urllib3"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools"
source="$pkgname-$pkgver.tar.gz::https://github.com/shazow/urllib3/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-urllib3" # Backwards compatibility
provides="py-urllib3=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	python3 setup.py check
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="95abcbdee8ecaf1dfb86da6c59e1eccce0524264f2b5ce5afb0f14fa577d5c5f255c022b5d51e127825d91f521f50fa2df8bca13b81b535ae3e6760a43be0cf6  py3-urllib3-1.25.11.tar.gz"
